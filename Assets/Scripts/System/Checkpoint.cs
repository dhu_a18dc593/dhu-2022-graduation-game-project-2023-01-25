using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    [SerializeField] bool isCheckPointUsed = false;

    private void OnTriggerEnter(Collider collider)
    {
        if(collider.gameObject.tag.Equals("Player") && !isCheckPointUsed)
        {
            isCheckPointUsed = true;
            GameManager gameManager = FindObjectOfType<GameManager>();
            gameManager.UpdateLastCheckPointName(gameObject.name);
        }
    }
}
