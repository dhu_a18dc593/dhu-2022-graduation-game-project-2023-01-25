using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BGMController : MonoBehaviour
{
    [SerializeField] AudioSource titleBGMAudioSource, stageBGMAudioSource;
    [SerializeField] float maxBGMVolumeFactor = 0.8f;

    private static bool created = false;
    private static bool playOPFlag = false;
    static int bgmVolume, seVolume;

    private void Awake()
    {
        Initialization();
    }

    private void Initialization()
    {
        if (PlayerPrefs.HasKey("BGMVolume"))
        {
            bgmVolume = PlayerPrefs.GetInt("BGMVolume");
        }
        else
        {
            bgmVolume = 100;
        }
        if (PlayerPrefs.HasKey("SEVolume"))
        {
            seVolume = PlayerPrefs.GetInt("SEVolume");
        }
        else
        {
            seVolume = 100;
        }
        titleBGMAudioSource.volume = maxBGMVolumeFactor * bgmVolume / 100;
        stageBGMAudioSource.volume = maxBGMVolumeFactor * bgmVolume / 100;
        if (!created)
        {
            // this is the first instance -make it persist
            DontDestroyOnLoad(this.gameObject);
            created = true;
        }
        else
        {
            // this must be aduplicate from a scene reload  - DESTROY!
            Destroy(this.gameObject);
        }
    }

    private void Update()
    {
        SwitchBGM();
        UpdateBGMVolume();
    }

    private void SwitchBGM()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        if (currentScene.name.Contains("Main") || currentScene.name.Contains("Cave"))
        {
            if (playOPFlag && (!titleBGMAudioSource.isPlaying))
            {
                    stageBGMAudioSource.Stop();
                    titleBGMAudioSource.Play();
            }
            else if (!stageBGMAudioSource.isPlaying)
            { 
                titleBGMAudioSource.Stop();
                stageBGMAudioSource.Play();
            }
        }
        else
        {
            if (!titleBGMAudioSource.isPlaying)
            {
                stageBGMAudioSource.Stop();
                titleBGMAudioSource.Play();
            }
        }
    }

    private void UpdateBGMVolume()
    {
        titleBGMAudioSource.volume = maxBGMVolumeFactor * bgmVolume / 100;
        stageBGMAudioSource.volume = maxBGMVolumeFactor * bgmVolume / 100;
    }

    public static void SetBGMVolume(int newBGMVolume)
    {
        bgmVolume = newBGMVolume;
    }

    public void PlayOpeningTheme()
    {
        playOPFlag = true;
    }
}
