using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChasePlayerArea : MonoBehaviour
{
    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Player"))
        {
            FindObjectOfType<MainSceneManager>().EnemiesStartChasingPlayer();
        }
    }
}
