using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FallOff : MonoBehaviour
{
    public Transform respawnPointPosition;

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Player"))
        {
            // FindObjectOfType<MainSceneManager>().RestartAtSpawnPoint(collider.gameObject.name, respawnPointPosition.GetComponent<Transform>().transform.position);
            /*
            ThirdPersonController player = collider.gameObject.GetComponent<ThirdPersonController>();
            player.DisableControl();
            player.StopAnimation();
            */
            FindObjectOfType<MainSceneManager>().DisablePlayerFollowCamera();
            FindObjectOfType<GameManager>().SetGameOverType("FallOff");
            Invoke("ProcessGameOver", 3f);
        }
        else if (collider.gameObject.tag.Equals("Enemy"))
        {
            Destroy(collider.gameObject, 1f);
        }
    }

    private void ProcessGameOver()
    {
        FindObjectOfType<MainSceneManager>().GameOver();
    }
}
