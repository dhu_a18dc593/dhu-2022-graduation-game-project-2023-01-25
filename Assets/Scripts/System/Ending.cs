using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ending : MonoBehaviour
{
    public GameObject titleButton;
    public float showTitleButtonSeconds = 5f;

    private void Start()
    {
        Initialization();
        StartCoroutine(ShowTitleButton());
    }

    private void Initialization()
    {
        titleButton.SetActive(false);
    }

    private IEnumerator ShowTitleButton()
    {
        yield return new WaitForSeconds(showTitleButtonSeconds);

        titleButton.SetActive(true);
    }

    public void ToTitle()
    {
        SceneManager.LoadScene("Title");
    }
}
