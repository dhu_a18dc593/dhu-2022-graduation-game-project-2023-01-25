using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatablePannie : MonoBehaviour
{
    public GameObject HiddenWall;
    public Quaternion openDoorTargetAngle;
    public Quaternion closeDoorTargetAngle;
    [SerializeField] float _rotateSpeed;

    [SerializeField] private bool openDoorFlag;

    private void Awake()
    {
        Initialization();
    }

    private void Initialization()
    {
        openDoorFlag = false;
        if(HiddenWall != null)
        {
            HiddenWall.GetComponent<BoxCollider>().enabled = false;
        }
    }

    private void Update()
    {
        if (openDoorFlag)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, openDoorTargetAngle, _rotateSpeed * Time.deltaTime);
            if(transform.rotation.z <= 0f && HiddenWall != null)
            {
                HiddenWall.GetComponent<BoxCollider>().enabled = true;
            }
        }
        else
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, closeDoorTargetAngle, _rotateSpeed * Time.deltaTime);
            if (transform.rotation.z >= -1f && HiddenWall != null)
            {
                HiddenWall.GetComponent<BoxCollider>().enabled = false;
            }
        }
    }

    public void OpenDoor()
    {
        openDoorFlag = true;
    }

    public void CloseDoor()
    {
        openDoorFlag = false;
    }
}
