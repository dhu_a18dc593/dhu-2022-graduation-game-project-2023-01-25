using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoloActionArea : MonoBehaviour
{
    public bool soloActionFlag, endOfSoloActionFlag;
    public BoxCollider[] restrictedAreas;

    private void OnTriggerStay(Collider collider)
    {
        if (collider.gameObject.name.Equals("YoungerBrother") && collider.gameObject.GetComponent<ThirdPersonController>().isActiveAndEnabled)
        {
            ThirdPersonController YoungerBrotherThirdPersonController = collider.gameObject.GetComponent<ThirdPersonController>();
            GameManager gameManager = FindObjectOfType<GameManager>();
            if (!YoungerBrotherThirdPersonController.IsInSoloAction())
            {
                if (endOfSoloActionFlag) {
                    gameManager.HideSoloActionEndHintText();
                    return; 
                }

                foreach (BoxCollider b in restrictedAreas)
                {
                    b.isTrigger = false;
                }
                YoungerBrotherThirdPersonController.EnableSoloAction();
                YoungerBrotherThirdPersonController.DisableConjuction();
                gameManager.ShowSoloActionStartHintText();
                gameManager.HideSoloActionEndHintText();
                if (soloActionFlag)
                {
                    foreach (BoxCollider b in restrictedAreas)
                    {
                        b.enabled = true;
                    }
                }
                else
                {
                    foreach (BoxCollider b in restrictedAreas)
                    {
                        b.enabled = false;
                    }
                }
            }
            else
            {
                YoungerBrotherThirdPersonController.EnableConjuction();
                gameManager.ShowSoloActionEndHintText();
                gameManager.HideSoloActionStartHintText();
                gameManager.HideSoloActionOnlyHintText();
                if (YoungerBrotherThirdPersonController.IsInSoloAction())
                {
                    foreach (BoxCollider b in restrictedAreas)
                    {
                        b.isTrigger = true;
                    }
                }
                else
                {
                    foreach (BoxCollider b in restrictedAreas)
                    {
                        b.isTrigger = false;
                    }
                }
            }
        }
        else if (collider.gameObject.GetComponent<ThirdPersonController>().isActiveAndEnabled)
        {
            GameManager gameManager = FindObjectOfType<GameManager>();
            gameManager.HideSoloActionStartHintText();
            gameManager.HideSoloActionEndHintText();
            if (soloActionFlag && !endOfSoloActionFlag)
            {
                gameManager.ShowSoloActionOnlyHintText();
            }
            else
            {
                gameManager.HideSoloActionOnlyHintText();
            }
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        GameManager gameManager = FindObjectOfType<GameManager>();
        if (collider.gameObject.name.Equals("YoungerBrother") && collider.gameObject.GetComponent<ThirdPersonController>().isActiveAndEnabled)
        {
            ThirdPersonController YoungerBrotherThirdPersonController = collider.gameObject.GetComponent<ThirdPersonController>();
            YoungerBrotherThirdPersonController.DisableSoloAction();
            YoungerBrotherThirdPersonController.DisableConjuction();
            gameManager.HideSoloActionStartHintText();
            gameManager.HideSoloActionEndHintText();
        }
        else
        {
            gameManager.HideSoloActionOnlyHintText();
        }
    }

    public void DisableSoloActionFlag()
    {
        soloActionFlag = false;
    }

    public void EnableEndOfSoloActionFlag()
    {
        endOfSoloActionFlag = true;
    }
}
