using Cinemachine;
using StarterAssets;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CaveManager : MonoBehaviour
{
    public static CaveManager Instance
    {
        get; private set;
    }

    public GameObject ItemCanvas, GameOverCanvas, HintCanvas, AnkaDialogCanvas, PauseCanvas;
    public GameObject ElderBrotherCharacter, YoungerBrotherCharacter, Bird, WaterDrop1;
    public GameObject Hint10Trigger1, Hint10Trigger2;
    public GameObject GameOverCheckPointButton, GameOverRestartButton;
    public GameObject PauseCheckPointButton, PauseResumeButton;
    public Animator openDoorAnimator;
    public Image AnkaDialogPanel, AnkaDialogImage, ladderImage;
    public RawImage bottleImage;
    public Text AnkaDialogText;
    public BoxCollider ExitCaveArea, OpenDoorArea;
    public NavMeshAgent ElderBrotherNavMeshAgent, YoungerBrotherNavMeshAgent, BirdNavMeshAgent;
    public AnimalCharacterAI birdAI;
    public ThirdPersonController ElderBrotherCharacterController, YoungerBrotherCharacterController;
    public PlayerInput ElderBrotherCharacterInput, YoungerBrotherCharacterInput;
    public FollowCharacterAI ElderBrotherCharacterFollowCharacterAI, YoungerBrotherCharacterFollowCharacterAI;
    public CinemachineVirtualCamera PlayerFollowCamera;
    public Transform OpenDoorAngle;
    public Transform ElderBrotherCameraRoot, YoungerBrotherCameraRoot;
    public Transform ControllerCharacterStartPosition, AICharacterStartPosition;
    public float ShowGameOverTextTime = 2f;
    public float GameOverRestartButtonPosX, GameOverRestartButtonPosY;
    public float GameOverCheckPointButtonPosX, GameOverCheckPointButtonPosY;
    public float PauseResumeButtonPosX, PauseResumeButtonPosY;
    public float PauseCheckPointButtonPosX, PauseCheckPointButtonPosY;

    private GameManager gameManager;
    private CaveSceneGameState CurrentGameState;
    private int hintsFromAnkaIndex;
    private string[] hintsFromAnka;

    enum CaveSceneGameState
    {
        GameStart,
        ShowHints,
        Pause,
        Win,
        GameOver,
    }

    public enum ControlCharacter
    {
        ElderBrother,
        YoungerBrother
    }

    // Start is called before the first frame update
    void Start()
    {
        if (Instance != null)
        {
            Destroy(this);
            return;
        }
        Instance = this;
        Initialization();
    }

    private void Initialization()
    {
        ItemCanvas.SetActive(true);
        GameOverCanvas.SetActive(false);
        HintCanvas.SetActive(true);
        PauseCanvas.SetActive(false);
        AnkaDialogPanel.enabled = false;
        AnkaDialogImage.enabled = false;
        AnkaDialogText.enabled = false;
        gameManager = FindObjectOfType<GameManager>();
        gameManager.GetAnkaDialog();
        CurrentGameState = CaveSceneGameState.GameStart;
        if (gameManager.IsOnlyYoungerBrotherAction())
        {
            ElderBrotherCharacterInput.enabled = false;
            ElderBrotherCharacterController.enabled = false;
            ElderBrotherCharacter.SetActive(false);
            Bird.SetActive(false);
            YoungerBrotherCharacterController.enabled = true;
            YoungerBrotherCharacterInput.enabled = false;
            YoungerBrotherCharacterInput.enabled = true;
            YoungerBrotherCharacterFollowCharacterAI.enabled = false;
            YoungerBrotherNavMeshAgent.enabled = false;
            SetCameraToFollowControlingPlayer("YoungerBrother");
        }
        else
        {
            if (gameManager.GetControlPlayerName() == GameManager.ControlCharacter.ElderBrother)
            {
                ElderBrotherCharacter.transform.position = ControllerCharacterStartPosition.position;
                YoungerBrotherCharacter.transform.position = AICharacterStartPosition.position;
                ElderBrotherCharacterFollowCharacterAI.enabled = false;
                YoungerBrotherCharacterFollowCharacterAI.enabled = true;
                ElderBrotherNavMeshAgent.enabled = false;
                YoungerBrotherNavMeshAgent.enabled = true;
                YoungerBrotherCharacterController.enabled = false;
                ElderBrotherCharacterController.DisableClimbing();
                ElderBrotherCharacterController.DisableChangeSceneFlag();
                YoungerBrotherCharacterInput.enabled = false;
                SetCameraToFollowControlingPlayer("ElderBrother");
                birdAI.SetTargetPlayer("YoungerBrother");
                DisableYoungerBrotherControl();
                EnableElderBrotherControl();
            }
            else
            {
                YoungerBrotherCharacter.transform.position = ControllerCharacterStartPosition.position;
                ElderBrotherCharacter.transform.position = AICharacterStartPosition.position;
                ElderBrotherCharacterFollowCharacterAI.enabled = true;
                YoungerBrotherCharacterFollowCharacterAI.enabled = false;
                ElderBrotherNavMeshAgent.enabled = true;
                YoungerBrotherNavMeshAgent.enabled = false;
                ElderBrotherCharacterController.enabled = false;
                YoungerBrotherCharacterController.enabled = true;
                YoungerBrotherCharacterController.DisableClimbing();
                YoungerBrotherCharacterController.DisableChangeSceneFlag();
                ElderBrotherCharacterInput.enabled = false;
                SetCameraToFollowControlingPlayer("YoungerBrother");
                birdAI.SetTargetPlayer("ElderBrother");
                DisableElderBrotherControl();
                EnableYoungerBrotherControl();
            }
        }
        if (gameManager.IsWaterDrop1Collected())
        {
            WaterDrop1.SetActive(false);
        }
        if (gameManager.CanUseBottle())
        {
            bottleImage.enabled = true;
        }
        if (gameManager.CanUseLadder())
        {
            ladderImage.enabled = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        switch (CurrentGameState)
        {
            case CaveSceneGameState.GameStart:
                HidePauseMenu();
                UpdateWaterDropCount();
                if (gameManager.IsEnteredFromBackMountain())
                {
                    ExitCaveArea.isTrigger = false;
                }

                if (gameManager.GetWaterDropCount() >= 3)
                {
                    Hint10Trigger1.SetActive(false);
                    Hint10Trigger2.SetActive(true);
                    OpenDoorArea.enabled = true;
                    OpenDoorArea.GetComponent<OpenDoor>().EnableOpenDoorFlag();
                }
                else
                {
                    Hint10Trigger1.SetActive(true);
                    Hint10Trigger2.SetActive(false);
                }
                break;
            case CaveSceneGameState.ShowHints:
                ProcessToShowDialog();
                break;
            case CaveSceneGameState.Pause:
                ShowPauseMenu();
                break;
            case CaveSceneGameState.Win:
                break;
            case CaveSceneGameState.GameOver:
                GameOver();
                break;
        }
    }

    private void GameOver()
    {
        StartCoroutine(ShowGameOverText());
        FindObjectOfType<ThirdPersonController>().enabled = false;
        foreach (PlayerInput p in FindObjectsOfType<PlayerInput>())
        {
            p.enabled = false;
        }
    }

    private IEnumerator ShowGameOverText()
    {
        yield return new WaitForSeconds(ShowGameOverTextTime);

        GameOverCanvas.SetActive(true);

        if (FindObjectOfType<GameManager>().GetLastCheckPointName().Equals(""))
        {
            GameOverCheckPointButton.SetActive(false);
            RectTransform rectTransform = GameOverRestartButton.GetComponent<RectTransform>();
            Vector2 anchoredPos = rectTransform.anchoredPosition;
            anchoredPos.x = GameOverCheckPointButtonPosX;
            anchoredPos.y = GameOverCheckPointButtonPosY;
            rectTransform.anchoredPosition = anchoredPos;
        }
        else
        {
            GameOverCheckPointButton.SetActive(true);
            RectTransform rectTransform = GameOverRestartButton.GetComponent<RectTransform>();
            Vector2 anchoredPos = rectTransform.anchoredPosition;
            anchoredPos.x = GameOverRestartButtonPosX;
            anchoredPos.y = GameOverRestartButtonPosY;
            rectTransform.anchoredPosition = anchoredPos;
        }

        InputSystem.EnableDevice(Mouse.current);
    }

    private void UpdateWaterDropCount()
    {
        gameManager.ShowWaterDropImage();
    }

    private void ShowPauseMenu()
    {
        PauseCanvas.SetActive(true);
        GameManager gameManager = FindObjectOfType<GameManager>();
        Debug.Log("gameManager.GetLastCheckPointName(); " + gameManager.GetLastCheckPointName());
        if (gameManager.GetLastCheckPointName().Equals(""))
        {
            PauseCheckPointButton.SetActive(false);
            RectTransform rectTransform = PauseResumeButton.GetComponent<RectTransform>();
            Vector2 anchoredPos = rectTransform.anchoredPosition;
            anchoredPos.x = PauseCheckPointButtonPosX;
            anchoredPos.y = PauseCheckPointButtonPosY;
            rectTransform.anchoredPosition = anchoredPos;
        }
        else
        {
            PauseCheckPointButton.SetActive(true);
            RectTransform rectTransform = PauseResumeButton.GetComponent<RectTransform>();
            Vector2 anchoredPos = rectTransform.anchoredPosition;
            anchoredPos.x = PauseResumeButtonPosX;
            anchoredPos.y = PauseResumeButtonPosY;
            rectTransform.anchoredPosition = anchoredPos;
        }
    }
    private void HidePauseMenu()
    {
        PauseCanvas.SetActive(false);
    }

    public void PauseGame()
    {
       Instance.ChangeCaveSceneState(CaveSceneGameState.Pause);
        GameManager gameManager = FindObjectOfType<GameManager>();
        if (gameManager.IsOnlyYoungerBrotherAction())
        {
            DisableYoungerBrotherControl();
        }
        else if (gameManager.GetControlPlayerName() == GameManager.ControlCharacter.ElderBrother)
        {
            DisableElderBrotherControl();
        }
        else
        {
            DisableYoungerBrotherControl();
        }
        InputSystem.EnableDevice(Mouse.current);
    }

    public void ResumeGame()
    {
        Instance.ChangeCaveSceneState(CaveSceneGameState.GameStart);
        GameManager gameManager = FindObjectOfType<GameManager>();
        if (gameManager.IsOnlyYoungerBrotherAction())
        {
            EnableYoungerBrotherControl();
        }
        else if (gameManager.GetControlPlayerName() == GameManager.ControlCharacter.ElderBrother)
        {
            EnableElderBrotherControl();
        }
        else
        {
            EnableYoungerBrotherControl();
        }
        InputSystem.DisableDevice(Mouse.current);
    }

    public void RestartAtCheckPoint()
    {
        FindObjectOfType<GameManager>().RestartAtCheckPointFromCave();
        SceneManager.LoadScene("MainScene");
    }

    public void RestartLevel()
    {
        FindObjectOfType<GameManager>().ResetAllData();
        SceneManager.LoadScene("MainScene");
    }

    public void RestartGame()
    {
        FindObjectOfType<GameManager>().ResetAllData();
        SceneManager.LoadScene("MainScene");
    }

    public void ToTitle()
    {
        FindObjectOfType<GameManager>().ResetAllData();
        SceneManager.LoadScene("Title");
    }

    public void ToEndingScreen()
    {
        FindObjectOfType<GameManager>().ResetAllData();
        SceneManager.LoadScene("Ending");
    }

    private void ProcessToShowDialog()
    {
        if (Keyboard.current.enterKey.wasPressedThisFrame)
        {
            if (hintsFromAnkaIndex + 1 >= hintsFromAnka.Length)
            {
                HideDialog();
                GameManager gameManager = FindObjectOfType<GameManager>();
                if (gameManager.GetControlPlayerName() == GameManager.ControlCharacter.ElderBrother)
                {
                    ElderBrotherCharacterInput.enabled = true;
                }
                else
                {
                    YoungerBrotherCharacterInput.enabled = true;
                }
                ChangeCaveSceneState(CaveSceneGameState.GameStart);
            }
            else
            {
                hintsFromAnkaIndex++;
                AnkaDialogText.text = hintsFromAnka[hintsFromAnkaIndex].Replace("|", Environment.NewLine);
            }
        }
    }

    public void HideDialog()
    {
        AnkaDialogCanvas.SetActive(false);
    }

    public void ShowDialog(string[] hintsContent, bool showOnceOnlyFlag = false)
    {
        AnkaDialogPanel.enabled = true;
        AnkaDialogImage.enabled = true;
        AnkaDialogText.enabled = true;
        hintsFromAnka = hintsContent;
        if (showOnceOnlyFlag)
        {
            hintsFromAnkaIndex = 0;
            AnkaDialogText.text = hintsContent[hintsFromAnkaIndex].Replace("|", Environment.NewLine);
            AnkaDialogCanvas.SetActive(true);
            AnkaDialogPanel.color = new Color(AnkaDialogPanel.color.r, AnkaDialogPanel.color.b, AnkaDialogPanel.color.b, 0.25f);
            GameManager gameManager = FindObjectOfType<GameManager>();
            if (gameManager.GetControlPlayerName() == GameManager.ControlCharacter.ElderBrother)
            {
                ElderBrotherCharacterInput.enabled = false;
            }
            else
            {
                YoungerBrotherCharacterInput.enabled = false;
            }
            ChangeCaveSceneState(CaveSceneGameState.ShowHints);
        }
        else
        {
            hintsFromAnkaIndex = 0;
            AnkaDialogText.text = hintsContent[hintsFromAnkaIndex].Replace("|", Environment.NewLine);
            AnkaDialogCanvas.SetActive(true);
            AnkaDialogPanel.color = new Color(AnkaDialogPanel.color.r, AnkaDialogPanel.color.b, AnkaDialogPanel.color.b, 0f);
        }
    }

    private void ChangeCaveSceneState(CaveSceneGameState targetGameState)
    {
        CurrentGameState = targetGameState;
    }

    public void ChangeControlPlayer(ControlCharacter player)
    {
        switch (player)
        {
            case ControlCharacter.ElderBrother:
                YoungerBrotherCharacterController.StopAnimation();
                YoungerBrotherCharacterController.enabled = false;
                YoungerBrotherCharacterInput.enabled = false;
                SetCameraToFollowControlingPlayer("ElderBrother");
                ElderBrotherCharacterController.enabled = true;
                ElderBrotherCharacterInput.enabled = true;
                ElderBrotherCharacterFollowCharacterAI.enabled = false;
                ElderBrotherNavMeshAgent.enabled = false;
                StartCoroutine(ChangeCharacterPosition("ElderBrother"));
                break;
            case ControlCharacter.YoungerBrother:
                ElderBrotherCharacterController.StopAnimation();
                ElderBrotherCharacterController.enabled = false;
                ElderBrotherCharacterInput.enabled = false;
                SetCameraToFollowControlingPlayer("YoungerBrother");
                YoungerBrotherCharacterController.enabled = true;
                YoungerBrotherCharacterInput.enabled = true;
                YoungerBrotherCharacterFollowCharacterAI.enabled = false;
                YoungerBrotherNavMeshAgent.enabled = false;
                StartCoroutine(ChangeCharacterPosition("YoungerBrother"));
                break;
        }

    }

    private IEnumerator ChangeCharacterPosition(string targetControlPlayer)
    {
        string targetAIPlayer = "";
        switch (targetControlPlayer)
        {
            case "ElderBrother":
                YoungerBrotherCharacterFollowCharacterAI.enabled = true;
                YoungerBrotherNavMeshAgent.enabled = true;
                YoungerBrotherCharacterFollowCharacterAI.MoveBehindControlPlayer();
                targetAIPlayer = "YoungerBrother";
                break;
            case "YoungerBrother":
                ElderBrotherCharacterFollowCharacterAI.enabled = true;
                ElderBrotherNavMeshAgent.enabled = true;
                Debug.Log("YoungerBrotherCharacter.transform.position: " + YoungerBrotherCharacter.transform.position);
                ElderBrotherCharacterFollowCharacterAI.MoveBehindControlPlayer();
                targetAIPlayer = "ElderBrother";
                break;
        }
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(ChangeAnimalPosition(targetAIPlayer));
    }

    public void SetCameraToFollowControlingPlayer(string targetCameraRoot)
    {
        switch (targetCameraRoot)
        {
            case "ElderBrother":
                PlayerFollowCamera.Follow = ElderBrotherCameraRoot;
                break;
            case "YoungerBrother":
                PlayerFollowCamera.Follow = YoungerBrotherCameraRoot;
                break;
        }
    }

    private IEnumerator ChangeAnimalPosition(string targetAIPlayer)
    {
        switch (targetAIPlayer)
        {
            case "ElderBrother":
                birdAI.SetTargetPlayer("ElderBrother");
                BirdNavMeshAgent.SetDestination(ElderBrotherCharacter.transform.position - new Vector3(0f, 0f, 1f));
                break;
            case "YoungerBrother":
                birdAI.SetTargetPlayer("YoungerBrother");
                BirdNavMeshAgent.SetDestination(YoungerBrotherCharacter.transform.position - new Vector3(0f, 0f, 1f));
                break;
        }
        yield return null;
    }

    public void ExitCave()
    {
        GameManager gameManager = FindObjectOfType<GameManager>();
        gameManager.ExitCave();
        Initiate.Fade("MainScene", Color.black, 1.0f);
    }

    private void DisableElderBrotherControl()
    {

        ElderBrotherCharacterController.enabled = false;
        ElderBrotherCharacterInput.enabled = false;
        ElderBrotherCharacterController.StopAnimation();
    }
    private void EnableElderBrotherControl()
    {

        ElderBrotherCharacterController.enabled = true;
        ElderBrotherCharacterController.EnableControl();
        ElderBrotherCharacterInput.enabled = true;
    }

    private void DisableYoungerBrotherControl()
    {

        YoungerBrotherCharacterController.enabled = false;
        YoungerBrotherCharacterInput.enabled = false;
        YoungerBrotherCharacterController.StopAnimation();
    }
    private void EnableYoungerBrotherControl()
    {

        YoungerBrotherCharacterController.enabled = true;
        YoungerBrotherCharacterController.EnableControl();
        YoungerBrotherCharacterInput.enabled = true;
    }

    public void OpenDoor()
    {
        PlayerFollowCamera.Follow = OpenDoorAngle;
        openDoorAnimator.SetTrigger("OpenDoor");
    }
}
