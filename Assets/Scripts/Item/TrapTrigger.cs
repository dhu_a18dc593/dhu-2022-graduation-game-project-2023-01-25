using Cinemachine;
using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapTrigger : MonoBehaviour
{
    public CinemachineVirtualCamera PlayerFollowCamera;
    public GameObject ElderBrotherCharacterCameraRoot, YoungerBroherCharacterCameraRoot;
    public GameObject trap, returnPos;
    public Transform cameraAngle;
    public string hint;
    public bool changeCameraAngle = true;
    public float activiateTrapMoveUpSeed, activiateMoveDownSpeed;
    public float trapOffsetY = 1f, trapDistance = 0.25f;
    public float originalShoulderOffsetY = 4.02f, targetShoulderOffsetY;
    public float returnToPosDistance = 0.5f, endTrapDistance = 0.95f;

    private bool isTrapActivated;

    private void Awake()
    {
        isTrapActivated = false;
    }

    private void OnTriggerStay(Collider collider)
    {
        if (collider.gameObject.name.Equals("ElderBrother") && !isTrapActivated && trap != null)
        {
            AnimalCharacterAI bird = FindObjectOfType<AnimalCharacterAI>();
            bird.SetActiviateTrapMoveSpeed(activiateTrapMoveUpSeed, activiateMoveDownSpeed);
            bird.SetTargetTrap(trap, this, trapOffsetY, trapDistance);
            bird.SetReturnToPlayerPosDistance(returnToPosDistance);
            bird.SetEndTrapDistance(endTrapDistance);
            FindObjectOfType<ThirdPersonController>().EnableActivateTrap();
            FindObjectOfType<GameManager>().ShowUseTrapHint();
            if (changeCameraAngle)
            {
                PlayerFollowCamera.Follow = cameraAngle;
            }
        }
        else
        {
            GameManager gameManager = FindObjectOfType<GameManager>();
            gameManager.HideUseTrapHint();
            if (gameManager.GetControlPlayerName() == GameManager.ControlCharacter.ElderBrother)
            {
                PlayerFollowCamera.Follow = ElderBrotherCharacterCameraRoot.transform;
            }
            else
            {
                PlayerFollowCamera.Follow = YoungerBroherCharacterCameraRoot.transform;
            }
        }
    }

    public void TrapActivated()
    {
        isTrapActivated = true;
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Player"))
        {
            GameManager gameManager = FindObjectOfType<GameManager>();
            gameManager.HideUseTrapHint();
            if (gameManager.GetControlPlayerName() == GameManager.ControlCharacter.ElderBrother)
            {
                PlayerFollowCamera.Follow = ElderBrotherCharacterCameraRoot.transform;
            }
            else
            {
                PlayerFollowCamera.Follow = YoungerBroherCharacterCameraRoot.transform;
            }
        }
    }
}
