using StarterAssets;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    private Rigidbody rb;
    private BoxCollider boxCollider;
    public string itemName;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        boxCollider = GetComponent<BoxCollider>();
    }
    public void EnableRagdoll()
    {
        rb.isKinematic = false;
        rb.detectCollisions = true;
    }
    public void DisableRagdoll()
    {
        rb.isKinematic = true;
        rb.detectCollisions = false;
    }

    private void OnCollisionEnter(Collision collider)
    {
        if (collider.gameObject.tag.Equals("Floor"))
        {
            rb.constraints = RigidbodyConstraints.FreezeAll;
            boxCollider.isTrigger = true;
        }
        if (collider.gameObject.tag.Equals("Player") && collider.gameObject.GetComponent<ThirdPersonController>().isActiveAndEnabled)
        {
            if (gameObject.tag.Equals("WaterDrop"))
            {
                if (FindObjectOfType<GameManager>().HasBottle())
                {
                    FindObjectOfType<GameManager>().PickItem(itemName);
                    Destroy(gameObject);
                }
                else
                {
                    FindObjectOfType<GameManager>().ShowCannotCollectWaterDropHint();
                }
            }
            else
            {
                FindObjectOfType<GameManager>().PickItem(itemName);
                Destroy(gameObject);
            }
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Player") && collider.gameObject.GetComponent<ThirdPersonController>().isActiveAndEnabled)
        {
            if (gameObject.tag.Equals("WaterDrop"))
            {
                if (FindObjectOfType<GameManager>().HasBottle())
                {
                    FindObjectOfType<GameManager>().PickItem(itemName);
                    Destroy(gameObject);
                }
                else
                {
                    FindObjectOfType<GameManager>().ShowCannotCollectWaterDropHint();
                }
            }
            else
            {
                FindObjectOfType<GameManager>().PickItem(itemName);
                Destroy(gameObject);
            }
        }
    }

    private void OnCollisionExit(Collision collider)
    {
        if (collider.gameObject.tag.Equals("Player") && gameObject.tag.Equals("WaterDrop"))
        {
            FindObjectOfType<GameManager>().HideCannotCollectWaterDropHint();
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Player") && gameObject.tag.Equals("WaterDrop"))
        {
            FindObjectOfType<GameManager>().HideCannotCollectWaterDropHint();
        }
    }


}
