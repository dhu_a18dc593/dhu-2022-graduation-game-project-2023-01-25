using UnityEngine;

public class Ladders : MonoBehaviour
{
    public bool isBroken = true;
    public GameObject LadderSeparate, LadderCombine;
    public ClimbLadderTrigger TargetClimbTrigger;

    private void Update()
    {
        if (isBroken)
        {
            LadderSeparate.SetActive(true);
            LadderCombine.SetActive(false);
        }
        else
        {
            LadderSeparate.SetActive(false);
            LadderCombine.SetActive(true);
        }
    }

    public void RepairLadder()
    {
        GameManager gameManager = FindObjectOfType<GameManager>();
        TargetClimbTrigger.EnableCanClimbFlag();
        gameManager.LadderGetRepaired(gameObject.name);
        gameManager.HideAnkaDialog();
        isBroken = false;
    }

    public void LadderIsRepaired()
    {
        isBroken = false;
        TargetClimbTrigger.EnableCanClimbFlag();
    }
}
