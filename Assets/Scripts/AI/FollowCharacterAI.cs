using StarterAssets;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

// 2022-05-11 by 楊
// 
public class FollowCharacterAI : MonoBehaviour
{
    public GameObject targetPlayer;
    public CharacterController controller;
    public Animator _animator;
    public RuntimeAnimatorController animatorControllerAI;
    public float moveSpeed = 2f;
    public float runSpeed = 5f;
    public float stealthWalkSpeed = 1f;
    public float crouchSpeed = 1.75f;
    public float followPlayerStoppingDistance = 1.0f;
    public float followPlayerStoppingDistanceOffset = 1f;
    public float distanceBetweenPlayer = 2.5f;
    public float faceTargetPlayerRotationSpeed = 1.0f;
    public float jumpAngle = 60.0f;
    public float jumpDuration = 1f;
    public float moveBehindControlPlayerDistance = 1f;
    public float navMeshAgentBaseOffset1 = -0.13f;

    public AudioClip LandingAudioClip;
    public AudioClip[] FootstepAudioClips;
    [Range(0, 1)] public float FootstepAudioVolume = 0.5f;

    [Tooltip("Acceleration and deceleration")]
    public float SpeedChangeRate = 10.0f;

    [Tooltip("The height the AI player can jump")]
    public float JumpHeight = 10f;

    [Tooltip("The character uses its own gravity value. The engine default is -9.81f")]
    public float Gravity = -15.0f;

    [Tooltip("How fast the character turns to face movement direction")]
    [Range(0.0f, 0.3f)]
    public float RotationSmoothTime = 0.12f;

    [Tooltip("Time required to pass before being able to jump again. Set to 0f to instantly jump again")]
    public float JumpTimeout = 0.50f;

    [Tooltip("デバッグレイの範囲")]
    public float DebugRayRange = 0.5f;

    [Tooltip("AIがジャンプできる範囲")]
    public float CanJumpRange = 0.5f;

    [Tooltip("Time required to pass before entering the fall state. Useful for walking down stairs")]
    public float FallTimeout = 0.15f;

    [Header("AI Player Grounded")]
    [Tooltip("Useful for rough ground")]
    public float GroundedOffset = -0.14f;

    [Tooltip("If the character is grounded or not. Not part of the CharacterController built in grounded check")]
    public bool Grounded = true;

    [Tooltip("The radius of the grounded check. Should match the radius of the CharacterController")]
    public float GroundedRadius = 0.28f;

    public float SmoothTime = 2f;

    [Tooltip("What layers the character uses as ground")]
    public LayerMask GroundLayers;

    // player
    private float _animationBlend;

    private bool _hasAnimator;
    private bool _canAIJump;

    // animation IDs
    private int _animIDSpeed;
    private int _animIDGrounded;
    private int _animIDJump;
    private int _animIDFreeFall;
    private int _animIDMotionSpeed;
    private int _animIDCrouchWalk;

    private float _startTime;
    private Vector3 _velocity = Vector3.zero;
    private Transform jumpOriginalPosition;
    private Transform jumpTargetPosition;

    private RaycastHit hit;
    private NavMeshAgent navMeshAgent;
    private new Rigidbody rigidbody;
    private bool m_isChangingPosition;
    private static bool m_isCrouching;
    private bool m_isJumping;

    // Start is called before the first frame update
    void Awake()
    {
        Initialization();
    }

    private void Initialization()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshAgent.autoTraverseOffMeshLink = false;
        navMeshAgent.speed = moveSpeed;
        rigidbody = GetComponent<Rigidbody>();
        controller = GetComponent<CharacterController>();
        _hasAnimator = TryGetComponent(out _animator);

        _animIDSpeed = Animator.StringToHash("Speed");
        _animIDGrounded = Animator.StringToHash("Grounded");
        _animIDJump = Animator.StringToHash("Jump");
        _animIDFreeFall = Animator.StringToHash("FreeFall");
        _animIDMotionSpeed = Animator.StringToHash("MotionSpeed");
        _animIDCrouchWalk = Animator.StringToHash("CrouchWalkSpeed");

        _canAIJump = false;
        m_isChangingPosition = false;
        m_isCrouching = false;
        m_isJumping = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (targetPlayer == null)
            return;

        if (!m_isChangingPosition)
        {
            FollowAnotherPlayer();
            AIGroundedCheck();
            AIJumpAndGravity();
        }
        else
        {
            MoveBehindControlCharacter();
        }
    }

    private void MoveBehindControlCharacter()
    {
        if (navMeshAgent.isActiveAndEnabled)
        {
            GameManager gameManager = FindObjectOfType<GameManager>();
            if (gameManager.IsInBackMountain())
            {
                navMeshAgent.SetDestination(targetPlayer.transform.position + new Vector3(0f, 0f, moveBehindControlPlayerDistance));
            }
            else
            {
                navMeshAgent.SetDestination(targetPlayer.transform.position - new Vector3(0f, 0f, moveBehindControlPlayerDistance));
            }
            float _controlCharacterSpeed = (navMeshAgent.remainingDistance > 2f) ? runSpeed : moveSpeed;
            navMeshAgent.speed = _controlCharacterSpeed;
            navMeshAgent.isStopped = false;


            if (m_isCrouching)
            {
                _animator.SetFloat(_animIDCrouchWalk, _animationBlend);
            }
            else
            {
                if (_hasAnimator)
                {
                    _animationBlend = Mathf.Lerp(_animationBlend, _controlCharacterSpeed / 4f, Time.deltaTime * SpeedChangeRate);
                    if (_animationBlend < 0.01f) _animationBlend = 0f;

                    _animator.SetFloat(_animIDSpeed, _animationBlend);
                    _animator.SetFloat(_animIDMotionSpeed, _controlCharacterSpeed);
                }
            }


            if (gameManager.IsInBackMountain())
            {
                if (Vector3.Distance(gameObject.transform.position, targetPlayer.transform.position + new Vector3(0f, 0f, moveBehindControlPlayerDistance)) <= 1.5f)
                {
                    navMeshAgent.speed = 0f;
                    navMeshAgent.isStopped = true;
                    m_isChangingPosition = false;
                    if (_hasAnimator)
                    {
                        _animator.SetFloat(_animIDSpeed, 0f);
                        _animator.SetFloat(_animIDMotionSpeed, 0f);
                    }
                }
            }
            else
            {
                if (Vector3.Distance(gameObject.transform.position, targetPlayer.transform.position - new Vector3(0f, 0f, moveBehindControlPlayerDistance)) <= 1.5f)
                {
                    navMeshAgent.speed = 0f;
                    navMeshAgent.isStopped = true;
                    m_isChangingPosition = false;
                    if (_hasAnimator)
                    {
                        _animator.SetFloat(_animIDSpeed, 0f);
                        _animator.SetFloat(_animIDMotionSpeed, 0f);
                    }
                }
            }
            
        }
    }

    protected void OnFootstep(AnimationEvent animationEvent)
    {
        if (animationEvent.animatorClipInfo.weight > 0.5f)
        {
            if (FootstepAudioClips.Length > 0)
            {
                var index = Random.Range(0, FootstepAudioClips.Length);
                if (FootstepAudioClips[index] != null && transform != null && FootstepAudioVolume > 0f)
                {
                    AudioSource.PlayClipAtPoint(FootstepAudioClips[index], transform.TransformPoint(controller.center), FootstepAudioVolume);
                }
            }
        }
    }

    public void ProcessCrouch()
    {
        Debug.Log(gameObject.name+ " AI ProcessCrouch Test");
        m_isCrouching = true;
        _animator.SetBool("Crouch", true);
    }
    public void ProcessStandUp()
    {
        m_isCrouching = false;
        _animator.SetBool("Crouch", false);
    }
    public void EnableAIAnimatorController()
    {
        _animator.runtimeAnimatorController = animatorControllerAI;
        if (m_isCrouching)
        {
            _animator.SetBool("Crouch", true);
        }
    }

    public void MoveBehindControlPlayer()
    {
        m_isChangingPosition = true;
    }

    private void FaceTarget(Vector3 destination)
    {
        Vector3 lookPos = destination - transform.position;
        lookPos.y = 0;
        Quaternion rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, faceTargetPlayerRotationSpeed);
    }

    private void AIGroundedCheck()
    {
        // set sphere position, with offset
        Vector3 spherePosition = new Vector3(transform.position.x, transform.position.y - GroundedOffset,
            transform.position.z);
        Grounded = Physics.CheckSphere(spherePosition, GroundedRadius, GroundLayers,
            QueryTriggerInteraction.Ignore);

        // update animator if using character
        if (_hasAnimator)
        {
            _animator.SetBool(_animIDGrounded, Grounded);
        }
    }

    public void SetJumpTargetPosition(Transform targetPos)
    {
        jumpOriginalPosition = transform;
        jumpTargetPosition = targetPos;
    }
    public void UnsetJumpTargetPosition()
    {
        jumpOriginalPosition = null;
        jumpTargetPosition = null;
    }


    private void AIJumpAndGravity()
    {
        if (m_isCrouching) { return; }

        if (Grounded)
        {
            rigidbody.isKinematic = true;
        }

        _canAIJump = (navMeshAgent.isOnOffMeshLink) ? true : false;

        if (_canAIJump)
        {
            StartCoroutine(StartAIJump());
            navMeshAgent.CompleteOffMeshLink();
        }
        else
        {
            _animator.SetBool(_animIDJump, false);
            if (navMeshAgent.isActiveAndEnabled)
            {
                navMeshAgent.isStopped = false;
            }
        }
    }

    private IEnumerator StartAIJump(string TargetAreaName = "")
    {
        _animator.SetBool(_animIDJump, true);
        OffMeshLinkData data;
        Vector3 startPos = navMeshAgent.transform.position;
        Vector3 endPos;
        if (!TargetAreaName.Equals(""))
        {
            endPos = GameObject.Find(TargetAreaName).transform.position;
        }
        else
        {
            data = navMeshAgent.currentOffMeshLinkData;
            endPos = data.endPos;
        }

        float time = 0.0f;

        while(time < 1.0f)
        {
            m_isJumping = true;
            float upDist = JumpHeight * 2f * (time - time * time);
            navMeshAgent.transform.position = Vector3.Lerp(startPos, endPos, time) + upDist * Vector3.up;

            time += Time.deltaTime / jumpDuration;
            yield return null;
        }
        m_isJumping = false;
    }

    public void JumpToGround(string TargetAreaName)
    {
        //StartCoroutine(StartAIJump(TargetAreaName));
        //navMeshAgent.CompleteOffMeshLink();
    }

    private void FollowAnotherPlayer()
    {
        if (navMeshAgent.isActiveAndEnabled)
        {
            if (Vector3.Distance(transform.position, targetPlayer.transform.position) >= distanceBetweenPlayer)
            {
                navMeshAgent.SetDestination(targetPlayer.transform.position);
            }

            if (Vector3.Distance(transform.position, targetPlayer.transform.position) <= distanceBetweenPlayer)
            {
                navMeshAgent.velocity = Vector3.zero;
                navMeshAgent.speed = 0f;
                navMeshAgent.isStopped = true;

                if (_hasAnimator)
                {
                    _animationBlend = 0f;

                    if (m_isCrouching)
                    {
                        _animator.SetFloat(_animIDCrouchWalk, 0f);
                    }
                    else
                    {
                        _animator.SetFloat(_animIDSpeed, _animationBlend);
                    }
                    _animator.SetFloat(_animIDMotionSpeed, 0f);
                }

            }
            else
            {
                float _characterAISpeed;
                ThirdPersonController player = targetPlayer.GetComponent<ThirdPersonController>();
                if (player.IsStealthWalking())
                {
                    _characterAISpeed = stealthWalkSpeed;
                }
                else if (player.IsWalking())
                {
                    _characterAISpeed = m_isCrouching ? crouchSpeed : runSpeed;
                }
                else if (m_isCrouching)
                {
                    _characterAISpeed = crouchSpeed;
                }
                else
                {
                    _characterAISpeed = 0f;
                }
                
                navMeshAgent.speed = _characterAISpeed;
                navMeshAgent.isStopped = false;

                if (_hasAnimator && Vector3.Distance(transform.position, targetPlayer.transform.position) > followPlayerStoppingDistance)
                {
                    _animationBlend = Mathf.Lerp(_animationBlend, _characterAISpeed / 4f, Time.deltaTime * SpeedChangeRate);
                    if (_animationBlend < 0.01f) _animationBlend = 0f;

                    if (m_isCrouching)
                    {
                        _animator.SetFloat(_animIDCrouchWalk, crouchSpeed);
                    }
                    else
                    {
                        _animator.SetFloat(_animIDSpeed, _animationBlend);
                    }
                    _animator.SetFloat(_animIDMotionSpeed, _characterAISpeed);
                }
            }
            if (!m_isJumping)
            {
                FaceTarget(targetPlayer.transform.position);
            }
        }
        else
        {
            _animator.SetFloat(_animIDSpeed, 0f);
            _animator.SetFloat(_animIDMotionSpeed, 0f);
        }
    }

    public void StartIdleing()
    {
        if (_hasAnimator)
        {
            _animator.SetFloat(_animIDSpeed, 0f);
            _animator.SetFloat(_animIDMotionSpeed, 0f);
        }
    }
}
