using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIJumpPoint : MonoBehaviour
{
    public Transform jumpTargetPos;
    public OffMeshLink offMeshLink;

    [SerializeField] Transform originalJumpTargetPos;

    private void Start()
    {
        originalJumpTargetPos = offMeshLink.endTransform;
    }

    private void OnTriggerStay(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Player"))
        {
            if (FindObjectOfType<ThirdPersonController>().IsJumpedToGround())
            {
                SetOffMeshLinkEndPosition("AIJumpWaypoint11");
                FindObjectOfType<MainSceneManager>().SetTargetRestoreAIJumpWaypoint(this);
            }
            if (collider.gameObject.GetComponent<FollowCharacterAI>().isActiveAndEnabled && jumpTargetPos != null)
            {
                collider.gameObject.GetComponent<FollowCharacterAI>().SetJumpTargetPosition(jumpTargetPos);
            }
            if(!offMeshLink.autoUpdatePositions)
            {
                offMeshLink.autoUpdatePositions = false;
            }
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Player"))
        {
            if (collider.gameObject.GetComponent<FollowCharacterAI>().isActiveAndEnabled && jumpTargetPos != null)
            {
                collider.gameObject.GetComponent<FollowCharacterAI>().UnsetJumpTargetPosition();
            }
        }
    }

    private void SetOffMeshLinkEndPosition(string targetJumpAreaName)
    {
        offMeshLink.endTransform = GameObject.Find(targetJumpAreaName).transform;
    }

    public void SetOffMeshLinkPosition(GameObject targetPosition)
    {
        offMeshLink.transform.position = targetPosition.transform.position;
    }

    public void RestoreOriginalJumpTargetPos()
    {
        offMeshLink.endTransform = originalJumpTargetPos;
    }
}
