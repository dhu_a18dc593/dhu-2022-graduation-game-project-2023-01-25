using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySensor : MonoBehaviour
{
    private Mesh mesh;
    public float viewRadius = 9f, viewAngle = 90;
    public float sensorHeight = 1.0f;
    public float meshResolution = 1f;
    public float searchPlayerDistance = 0.3f;
    public Color searchPlayerSpotColor = Color.red;

    private MeshFilter filter;

    // Start is called before the first frame update
    private void Awake()
    {
        mesh = CreateWedgeMesh();
        filter = GetComponent<MeshFilter>();
        filter.sharedMesh = mesh;
    }


    private Mesh CreateWedgeMesh()
    {
        Mesh mesh = new Mesh();

        int segments = 10;
        int numTriangles = (segments * 4) + 2 + 2;
        int numVertices = numTriangles * 3;

        Vector3[] vertices = new Vector3[numVertices];
        int[] triangles = new int[numVertices];

        Vector3 bottomCenter = Vector3.zero;
        Vector3 bottomLeft = Quaternion.Euler(0, -viewAngle, 0) * Vector3.forward * searchPlayerDistance;
        Vector3 bottomRight = Quaternion.Euler(0, viewAngle, 0) * Vector3.forward * searchPlayerDistance;

        Vector3 topCenter = bottomCenter + Vector3.up * sensorHeight;
        Vector3 topRight = bottomRight + Vector3.up * sensorHeight;
        Vector3 topLeft = bottomLeft + Vector3.up * sensorHeight;

        int vert = 0;

        // left side
        vertices[vert++] = bottomCenter;
        vertices[vert++] = bottomLeft;
        vertices[vert++] = topLeft;

        vertices[vert++] = topLeft;
        vertices[vert++] = topCenter;
        vertices[vert++] = bottomCenter;

        // right side
        vertices[vert++] = bottomCenter;
        vertices[vert++] = topCenter;
        vertices[vert++] = topRight;

        vertices[vert++] = topRight;
        vertices[vert++] = bottomRight;
        vertices[vert++] = bottomCenter;

        float currentAngle = -viewAngle;
        float deltaAngle = (viewAngle * 2) / segments;
        for(int i = 0; i < segments; ++i)
        {
            bottomLeft = Quaternion.Euler(0, currentAngle, 0) * Vector3.forward * searchPlayerDistance;
            bottomRight = Quaternion.Euler(0, currentAngle + deltaAngle, 0) * Vector3.forward * searchPlayerDistance;

            topRight = bottomRight + Vector3.up * sensorHeight;
            topLeft = bottomLeft + Vector3.up * sensorHeight; 
            
            // far side
            vertices[vert++] = bottomLeft;
            vertices[vert++] = bottomRight;
            vertices[vert++] = topRight;

            vertices[vert++] = topRight;
            vertices[vert++] = topLeft;
            vertices[vert++] = bottomLeft;

            // top
            vertices[vert++] = topCenter;
            vertices[vert++] = topLeft;
            vertices[vert++] = topRight;

            // bottom
            vertices[vert++] = bottomCenter;
            vertices[vert++] = bottomRight;
            vertices[vert++] = bottomLeft;

            currentAngle += deltaAngle;
        }

        for (int i = 0; i < numVertices; ++i)
        {
            triangles[i] = i;
        }

        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.RecalculateNormals();

        return mesh;
    }

    private void OnValidate()
    {
        mesh = CreateWedgeMesh();
    }

    private void OnDrawGizmos()
    {
        if (mesh)
        {
            Gizmos.color = searchPlayerSpotColor;
            Gizmos.DrawMesh(mesh, transform.position, transform.rotation);
        }
    }

    public void SetViewRadius(float viewRadius)
    {
        this.viewRadius = viewRadius;
    }

    public void SetSearchPlayerDistance(float searchDistance)
    {
        searchPlayerDistance = searchDistance;
    }
}
