using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using Cinemachine;
using StarterAssets;

public class Enemy : MonoBehaviour
{
    public NavMeshAgent navMeshAgent;
    public Animator animator;
    public GameObject partolPathObject, enemySensorObject;
    public GameObject chasePlayerDestination;
    public GameObject targetPlayer;
    public SpriteRenderer attractedImage;
    public GameObject attractedTimerSliderObject;
    public Image attractedTimerSlider;
    public bool noPatrol, canSeePlayerFlag = true, canBeAttackedByBird, dieFlag, cannotSeePlayerCrouchFlag = false;

    public int edgeIterations = 4;
    public float startWaitTime = 4f,  timeToRotate = 2f;
    public float speedWalk = 6f, speedRun = 9f;
    public float viewRadius = 9f, viewAngle = 90;
    public float sensorHeight = 1.0f;
    public float edgeDistance = 0.5f;
    public float searchPlayerDistance = 0.3f;
    public float stopChasingPlayerDistance = 1.5f;
    public float maxChasePlayerDistance = 6f;
    public float raycastYPosition = 1f;
    public float coolDownTime = 2f;
    public float fallOffForce = 3f;
    public float attractedByBirdTime = 15f;
    public float attractCountDownFactor = 0.075f;

    public LayerMask playerAndBirdMask, obstacleMask;

    [SerializeField] private bool m_AttackByBird;
    private List<Transform> waypoints;
    private int m_CurrentWaypointIndex;
    private Vector3 playerLastPosition,  m_PlayerPosition, searchPlayerSensorDirection;
    private float m_WaitTime, m_TimeToRotate, m_attractedTimer;
    private bool m_PlayerInRange, m_PlayerNear, m_BirdInRange;
    private bool m_IsPatrol, m_PlayerFound, m_BirdFound, m_PlayerIsCaught;
    private bool m_CoolDownFlag;
    private float m_CoolDownTimer;
    private CapsuleCollider bodyCollider;
    private Rigidbody rb;
    private string targetPlayerName;
    private EnemyState _currentEnemyState;
    private Coroutine coroutine;

    enum EnemyState
    {
        Normal,
        AttractedByBird,
        ChasePlayer,
        StopChasing,
        Die
    }

    private void Awake()
    {
        Initialization();
    }


    private void Initialization()
    {
        playerLastPosition = Vector3.zero;
        m_PlayerPosition = Vector3.zero;

        m_AttackByBird = false;
        m_IsPatrol = true;
        m_BirdFound = false;
        m_BirdInRange = false;
        m_PlayerFound = false;
        m_PlayerInRange = false;
        m_PlayerIsCaught = false;
        m_WaitTime = startWaitTime;
        m_TimeToRotate = timeToRotate;
        m_CurrentWaypointIndex = 0;
        m_attractedTimer = 0f;
        searchPlayerSensorDirection = Vector3.left;
        targetPlayerName = "";
        _currentEnemyState = EnemyState.Normal;
        m_CoolDownFlag = false;
        m_CoolDownTimer = 0f;

        bodyCollider = GetComponent<CapsuleCollider>();
        rb = GetComponent<Rigidbody>();
        waypoints = new List<Transform>();
        if(partolPathObject != null)
        {
            foreach (Transform childObj in
                partolPathObject.GetComponentInChildren<Transform>())
            {
                waypoints.Add(childObj.transform);
            }
        }

        navMeshAgent = GetComponent<NavMeshAgent>();
        if (navMeshAgent.isActiveAndEnabled && partolPathObject != null)
        {
            navMeshAgent.isStopped = false;
            navMeshAgent.speed = speedWalk;
            navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
        }

        if(attractedImage != null)
        {
            attractedImage.enabled = false ;
        }
        if(attractedTimerSliderObject != null)
        {
            attractedTimerSliderObject.SetActive(false);
        }
    }

    private void Update()
    {
        switch (_currentEnemyState)
        {
            case EnemyState.Normal:
                UpdateSearchPlayerDistanceAndViewRadius();
                if (noPatrol)
                {
                    if (canSeePlayerFlag && (!m_PlayerFound || !m_BirdFound))
                    {
                        EnvironmentView();
                    }

                    if (m_AttackByBird)
                    {
                        if (dieFlag)
                        {
                            // ChangeEnemyState(EnemyState.Die);
                            Die();
                            m_AttackByBird = false;
                        }
                        else
                        {
                            MoveToFallOff();
                        }
                    }
                    else
                    {
                        JustStanding();
                    }
                }
                else
                {
                    if (navMeshAgent == null || !navMeshAgent.isActiveAndEnabled || partolPathObject == null)
                    {
                        return;
                    }

                    if (!m_PlayerFound || !m_BirdFound)
                    {
                        EnvironmentView();
                    }

                    Patroling();
                }
                break;
            case EnemyState.AttractedByBird:
                BeingAttractedByBird();
                AttractByBirdCountDown();
                break;
            case EnemyState.ChasePlayer:
                EnvironmentView();
                StartChasingPlayer();
                break;
            case EnemyState.StopChasing:
                StopChasingPlayer();
                EnvironmentView();
                break;
            case EnemyState.Die:
                Die();
                break;
        }
    }

    private void StartChasingPlayer()
    {
        navMeshAgent.SetDestination(targetPlayer.transform.position);
        navMeshAgent.speed = speedRun;
        StartWalking();
        if (navMeshAgent.remainingDistance <= 1f)
        {
            StartIdle();
        }
        /*
        navMeshAgent.stoppingDistance = stopChasingPlayerDistance;
        if (Vector3.Distance(transform.position, targetPlayer.transform.position) <= stopChasingPlayerDistance && !m_PlayerIsCaught)
        {
            m_PlayerIsCaught = true;
            Debug.Log("Enemy Catch Player Test");
            navMeshAgent.speed = 0f;
            ChangeAnimationState("Catch");
            //Invoke("ProcessGameOver", 2.5f);
        }
        */
    }

    private void StopChasingPlayer()
    {
        navMeshAgent.speed = 0f;
        StartIdle();
    }

    private void UpdateSearchPlayerDistanceAndViewRadius()
    {
        if(enemySensorObject != null)
        {
            enemySensorObject.GetComponent<EnemySensor>().SetViewRadius(viewRadius);
            enemySensorObject.GetComponent<EnemySensor>().SetSearchPlayerDistance(searchPlayerDistance);
        }
    }

    private void JustStanding()
    {
        if (navMeshAgent == null || !navMeshAgent.isActiveAndEnabled) { return; }

        if (navMeshAgent.isActiveAndEnabled)
        {
            navMeshAgent.speed = 0f;
        }
        StartIdle();
    }

    public bool IsEnemyDieAtTheSameTime()
    {
        return dieFlag;
    }

    public void Die()
    {
        navMeshAgent.enabled = false;
        rb.constraints = RigidbodyConstraints.FreezePosition;
        bodyCollider.enabled = false;
        StartToDie();
        FindObjectOfType<GameManager>().EnemyDied(gameObject.name);
    }

    private void Patroling()
    {
        if (waypoints.Count <= 0) { return; }
        /*
        Vector3 rayPosition = transform.position + new Vector3(0f, raycastYPosition, 0f);
        Debug.DrawRay(rayPosition, searchPlayerSensorDirection * searchPlayerDistance, Color.red);
        */


        if (m_PlayerNear)
        {
            if(m_TimeToRotate <= 0)
            {
                Move(speedWalk);
                StartWalking();
                LookingPlayer(playerLastPosition);
            }
            else
            {
                Stop();
                StartIdle();
                m_TimeToRotate -= Time.deltaTime;
            }
        }
        else
        {
            StartWalking();
            m_PlayerNear = false;
            playerLastPosition = Vector3.zero; 
            navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
            if(navMeshAgent.speed == 0f)
            {
                navMeshAgent.speed = 1f;
            }
            if (navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance)
            {
                if(navMeshAgent.remainingDistance == 0.0f)
                {
                    Stop();
                    StartIdle();
                    m_TimeToRotate -= Time.deltaTime;
                }

                //Debug.Log("navMeshAgent_mWaitTime: " + m_WaitTime);
                if (m_WaitTime <= 0f)
                {
                    // ChangeFaceDirection();
                    NextPoint();
                    Move(speedWalk);
                    m_WaitTime = startWaitTime;
                }
                else
                {
                    m_WaitTime -= Time.deltaTime;
                }
            }
        }
    }

    public void ChasePlayer()
    {
        ChangeEnemyState(EnemyState.ChasePlayer);
    }

    public void StopChasing()
    {
        ChangeEnemyState(EnemyState.StopChasing);
    }

    private void LookingPlayer(Vector3 player)
    {
        if (waypoints.Count <= 0) { return; }

        navMeshAgent.SetDestination(player);
        if (Vector3.Distance(transform.position, player) <= searchPlayerDistance)
        {
            if(m_WaitTime <= 0f)
            {
                m_PlayerNear = false;
                Move(speedWalk);
                StartWalking();
                navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
                m_WaitTime = startWaitTime;
                m_TimeToRotate = timeToRotate;
            }
            else
            {
                Stop();
                StartIdle();
                m_WaitTime -= Time.deltaTime;
            }
        }
    }

    private void Move(float speed)
    {
        navMeshAgent.isStopped = false;
        navMeshAgent.speed = speed;
    }

    public void AttackedByBird()
    {
        m_AttackByBird = true;
    }

    private void MoveToFallOff()
    {
        StartWalking();
        rb.AddForce(Vector3.forward * fallOffForce, ForceMode.Impulse);
    }

    public void Stop()
    {
        if(navMeshAgent.isActiveAndEnabled)
        {
            navMeshAgent.speed = 0f;
        }
    }

    public void RestorePreviousState()
    {
        if (!noPatrol)
        {
            if (navMeshAgent.isActiveAndEnabled)
            {
                navMeshAgent.speed = 1f;
                StartWalking();
            }
        }
    }

    public void NextPoint()
    {
        m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Count;
        navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
    }

    private void EnvironmentView()
    {
        if (m_CoolDownFlag)
        {
            m_CoolDownTimer += Time.deltaTime;
            if(m_CoolDownTimer >= coolDownTime)
            {
                m_CoolDownFlag = false;
                m_CoolDownTimer = 0f;
            }
        }
        Collider[] PlayerAndBirdInRange = Physics.OverlapSphere(transform.position, viewRadius, playerAndBirdMask);
        Vector3 rayPosition = transform.position + new Vector3(0f, raycastYPosition, 0f);
        Debug.DrawRay(rayPosition, searchPlayerSensorDirection * searchPlayerDistance, Color.red);
        for (int i = 0; i < PlayerAndBirdInRange.Length; i++)
        {
            Transform PlayerAndBird = PlayerAndBirdInRange[i].transform;
            Vector3 dirToPlayerAndBird = (PlayerAndBird.position - transform.position).normalized;

            // 兄または弟
            if (Vector3.Angle(transform.forward, dirToPlayerAndBird) < viewAngle / 4)
            {
                float dstToPlayer = Vector3.Distance(transform.position, PlayerAndBird.position);
                if (!Physics.Raycast(transform.position, dirToPlayerAndBird, dstToPlayer, obstacleMask))
                {
                    Debug.Log("Enemy Scanned Character: "+ PlayerAndBirdInRange[i].gameObject.name);
                    if (PlayerAndBird.gameObject.tag.Equals("Bird"))
                    {
                        m_PlayerInRange = false;
                        m_BirdInRange = true;
                    }
                    else if (PlayerAndBird.gameObject.tag.Equals("Player"))
                    {
                        m_PlayerInRange = PlayerAndBird.gameObject.GetComponent<ThirdPersonController>().IsCrouching() ? false : true;
                        if(targetPlayer == null)
                        {
                            targetPlayer = PlayerAndBird.gameObject;
                        }
                        m_BirdInRange = false;
                    }
                    m_IsPatrol = false;
                }
                else
                {
                    m_PlayerInRange = false;
                    m_BirdInRange = false;
                }
            }
            // アンカ
            else if (Vector3.Angle(transform.forward, dirToPlayerAndBird) < viewAngle / 2)
            {
                float dstToPlayer = Vector3.Distance(transform.position, PlayerAndBird.position);
                if (!Physics.Raycast(transform.position, dirToPlayerAndBird, dstToPlayer, obstacleMask) && !m_CoolDownFlag)
                {
                    m_BirdInRange = true;
                    m_IsPatrol = false;
                }
                else
                {
                    m_BirdInRange = false;
                }
            }

            if (Vector3.Distance(transform.position, PlayerAndBird.position) > viewRadius)
            {
                m_PlayerInRange = false;
                m_BirdInRange = false;
            }
            if (m_PlayerInRange)
            {
                m_PlayerFound = true;
                PlayerFound();
            }
            else if (m_BirdInRange && FindObjectOfType<AnimalCharacterAI>().IsAttractingEnemies())
            {
                m_BirdFound = true;
                m_attractedTimer = 0f;
                ChangeEnemyState(EnemyState.AttractedByBird);
            }
        }
    }

    public bool CanbeAttackedByBird()
    {
        return canBeAttackedByBird;
    }

    public void StartAttractedByBird()
    {
        ChangeEnemyState(EnemyState.AttractedByBird);
    }

    public void StopAttractedByBird()
    {
        if (attractedImage != null)
        {
            attractedImage.enabled = false;
        }
        if (attractedTimerSliderObject != null)
        {
            attractedTimerSliderObject.SetActive(false);
        }
        navMeshAgent.speed = speedWalk;
        m_BirdInRange = false;
        m_BirdFound = false;
        m_IsPatrol = true;
        m_CoolDownFlag = true;
        ChangeEnemyState(EnemyState.Normal);
    }

    public bool DiscoveredBird()
    {
        return m_BirdFound == true;
    }

    public bool IsAttractedByBird()
    {
        return _currentEnemyState == EnemyState.AttractedByBird;
    }

    private void ChangeEnemyState(EnemyState state)
    {
        _currentEnemyState = state;
    }

    public void ChangeAnimationState(string state)
    {
        switch (state)
        {
            case "Idle":
                animator.SetBool("IsWalking", false);
                break;
            case "Walking":
                animator.SetBool("IsWalking", true);
                break;
            case "Die":
                animator.SetTrigger("Die");
                break;
            case "Catch":
                animator.SetTrigger("Catch");
                break;
            case "AfterCatch":
                animator.SetTrigger("AfterCatch");
                break;
        }
    }

    private void StartWalking()
    {
        ChangeAnimationState("Walking");
    }

    private void StartIdle()
    {
        ChangeAnimationState("Idle");
    }

    private void StartToDie()
    {
        ChangeAnimationState("Die");
    }

    private void BeingAttractedByBird()
    {
        AnimalCharacterAI bird = FindObjectOfType<AnimalCharacterAI>();
        navMeshAgent.SetDestination(bird.transform.position - new Vector3(0f, 0f, 1f));
        if(navMeshAgent.remainingDistance < 1f)
        {
            navMeshAgent.speed = 0f;
            StartIdle();
        }
        else
        {
            navMeshAgent.speed = speedWalk;
            StartWalking();
        }

        if (attractedImage != null)
        {
            attractedImage.enabled = true;
        }
        if (attractedTimerSliderObject != null)
        {
            attractedTimerSliderObject.SetActive(true);
        }

        if (m_attractedTimer > attractedByBirdTime)
        {
            StopAttractedByBird();
        }
    }

    private void AttractByBirdCountDown()
    {
        float fillAmountFactor;
        if (attractedTimerSlider.isActiveAndEnabled)
        {
            attractedTimerSlider.color = Color.blue;
            m_attractedTimer += Time.deltaTime;
            fillAmountFactor = (attractedByBirdTime - m_attractedTimer) / attractedByBirdTime;
            attractedTimerSlider.fillAmount = fillAmountFactor;
            if(fillAmountFactor >= 0.2f && fillAmountFactor <= 0.5f)
            {
                attractedTimerSlider.color = Color.yellow;
            }
            else if (fillAmountFactor < 0.2f)
            {
                attractedTimerSlider.color = Color.red;
            }
        }
    }

    private void PlayerFound()
    {
        foreach (ThirdPersonController c in FindObjectsOfType<ThirdPersonController>())
        {
            c.DisableControl();
            c.StopAnimation();
        }
        //ChangeEnemyState(EnemyState.ChasePlayer);
        FindObjectOfType<MainSceneManager>().GameOver();
    }

    public bool IsPlayerFound()
    {
        return m_PlayerFound;
    }

    public void ProcessGameOver()
    {
        ChangeAnimationState("AfterCatch");
        targetPlayer = null;
        m_PlayerIsCaught = false;
        ChangeEnemyState(EnemyState.Normal);
        FindObjectOfType<MainSceneManager>().GameOver();
    }
}
