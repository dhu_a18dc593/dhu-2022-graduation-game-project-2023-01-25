using UnityEngine;
#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
using UnityEngine.InputSystem;
#endif

namespace StarterAssets
{
	public class StarterAssetsInputs : MonoBehaviour
	{
		[Header("Character Input Values")]
		public Vector2 move;
		public Vector2 look;
		public bool jump;
		public bool sprint;
		public bool slowDown;
		public bool pickUp;
		public bool dieTest;
		public bool getHitTest;
		public bool hideCharacter;
		public bool climb;
		public bool changeCharacter;
		public bool activateTrap;
		public bool attackEnemy;
		public bool useItem;
		public bool crouch;
		public bool attractEnemy;
		public bool soloAction;
		public bool changeSceneArea;
		public bool openDoor;
		public bool pause;

		[Header("Movement Settings")]
		public bool analogMovement;

		[Header("Mouse Cursor Settings")]
		public bool cursorLocked = true;
		public bool cursorInputForLook = true;

		private ThirdPersonController ThirdPersonController;
		private bool m_SoloAction;

        private void Awake()
        {
            Initialization();
        }

        private void Initialization()
        {
            crouch = false;
			ThirdPersonController = GetComponent<ThirdPersonController>();
			m_SoloAction = false;
		}


#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
        public void OnMove(InputValue value)
		{
			MoveInput(value.Get<Vector2>());
		}

		public void OnLook(InputValue value)
		{
			if(cursorInputForLook)
			{
				LookInput(value.Get<Vector2>());
			}
		}

		public void OnJump(InputValue value)
		{
			JumpInput(value.isPressed);
		}

		public void OnSprint(InputValue value)
		{
			SprintInput(value.isPressed);
		}
		public void OnSlowMove(InputValue value)
		{
			SlowDownInput(value.isPressed);
		}

		public void OnPickUp(InputValue value)
        {
			PickUpInput(value.isPressed);
        }

		public void OnHideCharacter(InputValue value)
        {
			HideCharacterInput(value.isPressed);
        }

		public void OnClimb(InputValue value)
        {
			ClimbInput(value.isPressed);
		}

		public void OnChangeCharacter(InputValue value)
        {
			ChangeCharacterInput(value.isPressed);
        }

		public void OnActivateTrap(InputValue value)
        {
			ActivateTrapInput(value.isPressed);
        }

		public void OnAttackEnemy(InputValue value)
        {
			AttackEnemyInput(value.isPressed);
        }

		public void OnUseItem(InputValue value)
        {
			UseItemInput(value.isPressed);
        }

		public void OnCrouch()
		{
			CrouchInput();
		}

		public void OnAttractEnemy()
        {
			AttractEnemyInput();
        }

		public void OnSoloAction()
        {
			SoloActionInput();
        }

		public void OnChangeSceneArea(InputValue value)
        {
			ChangeSceneAreaInput(value.isPressed);
		}

		public void OnOpenDoor(InputValue value)
        {
			OpenDoorInput(value.isPressed);
        }

		public void OnPause(InputValue value)
        {
			PauseInput(value.isPressed);
        }
#endif

		public void MoveInput(Vector2 newMoveDirection)
		{
			move = newMoveDirection;
		} 

		public void LookInput(Vector2 newLookDirection)
		{
			look = newLookDirection;
		}

		public void JumpInput(bool newJumpState)
		{
			jump = newJumpState;
		}

		public void SprintInput(bool newSprintState)
		{
			sprint = newSprintState;
		}

		public void SlowDownInput(bool newSlownState)
        {
			slowDown = newSlownState;
        }

		public void PickUpInput(bool newPickUpState)
        {
			pickUp = newPickUpState;
        }

		public void HideCharacterInput(bool newHideCharacterState)
        {
			hideCharacter = newHideCharacterState;
        }

		public void ChangeCharacterInput(bool newChangeCharacterState)
        {
			changeCharacter = newChangeCharacterState;
        }

		public void ClimbInput(bool newClimbState)
        {
			climb = newClimbState;
            if (climb)
            {
				ThirdPersonController.ProcessClimbLadder();
			}
        }

		public void ActivateTrapInput(bool newActivateTrapState)
        {
			activateTrap = newActivateTrapState;
            if (activateTrap && ThirdPersonController.CanActiviateTrap() && gameObject.name.Equals("ElderBrother"))
            {
				ThirdPersonController.ProcessActivateTrap();
            }
        }

		public void AttackEnemyInput(bool newAttackEnemyState)
        {
			attackEnemy = newAttackEnemyState;
        }

		public void UseItemInput(bool newUseItemState)
        {
			useItem = newUseItemState;
        }

		public void CrouchInput()
        {
			crouch = !crouch;
            if (crouch)
            {
				ThirdPersonController.ProcessCrouch();
            }
            else if(ThirdPersonController.CanStandUp())
            {
				ThirdPersonController.ProcessStandUp();
			}
        }

		public void AttractEnemyInput()
        {
			attractEnemy = !attractEnemy;
            if (attractEnemy)
            {
				ThirdPersonController.ProcessAttractEnemy();
            }
            else
			{
				ThirdPersonController.ProcessAttractEnd();
			}
        }

		public void SoloActionInput()
        {
			if (!m_SoloAction && !ThirdPersonController.IsInSoloAction() && 
				ThirdPersonController.CanSoloAction() && gameObject.name.Equals("YoungerBrother"))
			{
				m_SoloAction = true;
				ThirdPersonController.SoloAction();
			}
			else if(m_SoloAction && ThirdPersonController.CanConjunction() && ThirdPersonController.IsInSoloAction())
			{
				m_SoloAction = false;
				ThirdPersonController.Conjuction();
			}
        }

		public void ChangeSceneAreaInput(bool newChangeSceneAreaState)
        {
			changeSceneArea = newChangeSceneAreaState;
			if (changeSceneArea)
			{
				ThirdPersonController.ProcessChangeScene();
			}
		}

		public void InitSoloActionToTrue()
        {
			m_SoloAction = true;
		}

		public void OpenDoorInput(bool newOpenDoorState)
        {
			openDoor = newOpenDoorState;
			if (openDoor)
			{
				ThirdPersonController.ProcessOpenDoor();
			}
		}

		public void PauseInput(bool newPauseState)
        {
			pause = newPauseState;
            if (pause)
            {
				ThirdPersonController.ProcessPauseGame();

			}
        }
	}
}